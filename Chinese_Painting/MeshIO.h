#ifndef __MESHIO_H
#define __MESHIO_H
#include <stdlib.h>
#include "Mesh.h"
#include "ply.h"

typedef struct Vertex {
	float x, y, z, nx, ny, nz;             /* the usual 3-space position of a vertex */
} Vertex;

typedef struct Triangle {
	unsigned char nverts;    /* number of vertex indices in list */
	int *verts;              /* vertex index list */
} Triangle;

/* information needed to describe the user's data to the PLY routines */

char *elem_names[] = { /* list of the kinds of elements in the user's object */
	"vertex", "face"
};

PlyProperty vert_props[] = { /* list of property information for a vertex */
		{ "x", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, x), 0, 0, 0, 0 },
		{ "y", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, y), 0, 0, 0, 0 },
		{ "z", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, z), 0, 0, 0, 0 },
		{ "nx", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, nx), 0, 0, 0, 0 },
		{ "ny", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, ny), 0, 0, 0, 0 },
		{ "nz", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, nz), 0, 0, 0, 0 },
};

PlyProperty face_props[] = { /* list of property information for a vertex */
		{ "vertex_indices", PLY_INT, PLY_INT, offsetof(Triangle, verts),
		1, PLY_UCHAR, PLY_UCHAR, offsetof(Triangle, nverts) },
};

class MeshIO
{
public:
	static int readPlyFile(char *fileName, Mesh *pMesh){
		/* Error Definition:
		 * -1: Null pointer detected
		 * -2: Cannot find file
		 */
		if (pMesh == NULL || fileName == NULL){
			printf("Error: NULL pointer detected");
			return -1;
		}
		int i, j, k;
		PlyFile *ply;
		int nelems;
		char **elist;
		int file_type;
		float version;
		int nprops;
		int num_elems;
		PlyProperty **plist;
		Vertex **vlist;
		Triangle **flist;
		char *elem_name;

		/* open a PLY file for reading */
		ply = ply_open_for_reading(fileName , &nelems, &elist, &file_type, &version);
		if (ply == NULL){
			printf("Error: cannot find file: %s", fileName);
			return -2;
		}
		printf("File name: %s\n", fileName);

		/* print what we found out about the file */
		//printf("version %f\n", version);
		//printf("type %d\n", file_type);

		/* go through each kind of element that we learned is in the file */
		/* and read them */

		for (i = 0; i < nelems; i++) {

			/* get the description of the first element */
			elem_name = elist[i];
			plist = ply_get_element_description(ply, elem_name, &num_elems, &nprops);

			/* print the name of the element, for debugging */
			printf("Element %s %d\n", elem_name, num_elems);

			/* if we're on vertex elements, read them in */
			if (equal_strings("vertex", elem_name)) {
				/* create the vertex buffer inside the vertices */
				pMesh->pVertexBuffer = new float[3 * num_elems]();
				pMesh->pNormalBuffer = new float[3 * num_elems]();
				pMesh->numVertex = num_elems;
				pMesh->numNormal = num_elems;
				/* create a vertex list to hold all the vertices (and the normals)*/
				vlist = (Vertex **)malloc(sizeof(Vertex *) * num_elems);
				/* set up for getting vertex elements */

				ply_get_property(ply, elem_name, &vert_props[0]); // x
				ply_get_property(ply, elem_name, &vert_props[1]); // y
				ply_get_property(ply, elem_name, &vert_props[2]); // z
				ply_get_property(ply, elem_name, &vert_props[3]); // nx
				ply_get_property(ply, elem_name, &vert_props[4]); // ny
				ply_get_property(ply, elem_name, &vert_props[5]); // nz

				/* grab all the vertex elements */
				for (j = 0; j < num_elems; j++) {

					/* grab and element from the file */
					vlist[j] = (Vertex *)malloc(sizeof(Vertex));
					ply_get_element(ply, (void *)vlist[j]);

					/* copy the data from Vertex data to the Mesh object*/
					pMesh->pVertexBuffer[j * 3] = vlist[j]->x;
					pMesh->pVertexBuffer[j * 3 + 1] = vlist[j]->y;
					pMesh->pVertexBuffer[j * 3 + 2] = vlist[j]->z;
					pMesh->pNormalBuffer[j * 3] = vlist[j]->nx;
					pMesh->pNormalBuffer[j * 3 + 1] = vlist[j]->ny;
					pMesh->pNormalBuffer[j * 3 + 2] = vlist[j]->nz;
				}
			}

			/* if we're on face elements, read them in */
			if (equal_strings("face", elem_name)) {
				/* create the index buffer inside the Mesh object */
				pMesh->pIndexBuffer = new unsigned short[3 * num_elems]();
				pMesh->numIndex = num_elems;
				/* create a list to hold all the face elements */
				flist = (Triangle **)malloc(sizeof(Triangle *) * num_elems);

				/* set up for getting face elements */

				ply_get_property(ply, elem_name, &face_props[0]);

				/* grab all the face elements */
				for (j = 0; j < num_elems; j++) {

					/* grab and element from the file */
					flist[j] = (Triangle *)malloc(sizeof(Triangle));
					ply_get_element(ply, (void *)flist[j]);

					/* copy the index data into the buffer*/
					pMesh->pIndexBuffer[j * 3] = flist[j]->verts[0];
					pMesh->pIndexBuffer[j * 3 + 1] = flist[j]->verts[1];
					pMesh->pIndexBuffer[j * 3 + 2] = flist[j]->verts[2];
				}
			}
		}
		/* close the PLY file */
		ply_close(ply);
	}
};

#endif