#ifndef __TEXTURE_IO_H
#define __TEXTURE_IO_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "glew\glew.h"
class TextureIO
{
public:
	static GLuint loadBMP_custom(const char * imagepath);
};
#endif

