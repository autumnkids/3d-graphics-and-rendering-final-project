#ifndef __MESH_H
#define __MESH_H
#include <stdio.h>
/* Mesh provides raw access to vertex, normal and else mesh related data
* Currently, only vertex, normal and index are supported
*/
class Mesh
{
public:
	Mesh();
	~Mesh();
	int numVertex;
	int numNormal;
	int numIndex;
	float *pVertexBuffer;
	float *pNormalBuffer;
	unsigned short *pIndexBuffer;
};
#endif

