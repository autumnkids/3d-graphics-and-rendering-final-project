#include "Chinese_Painting.h"
#include "input.h"
#include "MeshIO.h"
#include "TextureIO.h"
#include "glew\wglew.h"
// camera parameters
double Theta = pi / 6;
double Phi = 0;
double R = 8;

// mouse control
int g_iMenuId;
int g_vMousePos[2];
int g_iLeftMouseButton, g_iMiddleMouseButton, g_iRightMouseButton;

int windowWidth;
int windowHeight;

// GPU IDs
GLuint programID_1;
GLuint programID_2;
GLuint programID_3;
GLuint programID_current;
GLuint programID_tex;

// Buffers
GLuint vertexbuffer;
GLuint normalbuffer;
GLuint elementbuffer;
GLuint bgvertexbuffer;
GLuint bguvbuffer;
GLuint Texture;
GLuint VertexArrayID;
// Uniform ID for texture
GLuint TextureID;
// Matrix for unifom variable
GLuint MatrixID_mvp;
GLuint MatrixID_normal;
glm::mat4 mvpMatrix;
glm::mat4 normalMatrix;
//
Mesh bunnyMesh;

void reshape(int w, int h)
{
	/* I comment out most part of this because I introduce a new math library glm which
	 * deals with the vector and matrix. So it would be better if we rewrite the code for this part
	 */

	//Prevent a divide by zero, when h is zero.
	//You can't make a window of zero height.
	//if(h == 0)
	//  h = 1;

	//glViewport(0, 0, w, h);

	//// Reset the coordinate system before modifying
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//// Set the perspective
	//double aspectRatio = 1.0 * w / h;
	//gluPerspective(60.0f, aspectRatio, 0.01f, 1000.0f);

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity(); 

	//windowWidth = w;
	//windowHeight = h;

	glutPostRedisplay();
}

void myinit()
{
	// set background color to grey
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	printf("%s\n", glGetString(GL_VERSION));
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE_ARB);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_LINE_SMOOTH);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	// test
	GLint buf[10];
	GLint sbuf[10];
	glGetIntegerv(GL_SAMPLE_BUFFERS_ARB, buf);
	printf("number of sample buffers is %d\n", buf[0]);
	glGetIntegerv(GL_SAMPLES_ARB, sbuf);
	printf("number of samples is %d\n", sbuf[0]);

	// set the background data
	// const buffer for background
	static const GLfloat bg_vertex_buffer[] = {
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
	};

	static const GLfloat bg_uv_buffer[] = {
			0.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
	};

	// set the light
	GLfloat lP[4] = {0, 1.0, 1.0, 1.0};
	GLfloat lKa[4] = {0.3, 0.3, 0.3, 1.0};
	GLfloat lKd[4] = {0.9, 0.2, 0.3, 1.0};
	GLfloat lKs[4] = {0.9, 0.2, 0.3, 1.0};

	glLightfv(GL_LIGHT0, GL_POSITION, lP);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lKa);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lKd);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lKs);
	glEnable(GL_LIGHT0);

	// enable lighting
	glEnable(GL_LIGHTING);


	// Load the mesh from the ply file
	int result = MeshIO::readPlyFile("./model/bunny_normal.ply", &bunnyMesh);
	if (result != 0){
		exit(-1);
	}
	// initialize the glew library
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		exit(-1);
	}
	// initialization here
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	programID_1 = LoadShaders("./shader/common.vertexshader", "./shader/interior.fragmentshader");
	programID_2 = LoadShaders("./shader/common.vertexshader", "./shader/silhouette.fragmentshader");
	programID_3 = LoadShaders("./shader/common.vertexshader", "./shader/final.fragmentshader");
	programID_tex = LoadShaders("./shader/background.vertexshader", "./shader/background.fragmentshader");
	programID_current = programID_2;

	// Load the texture
	Texture = TextureIO::loadBMP_custom("./tex/xuan_paper.bmp");

	// Get a handle for our "MVP" uniform
	MatrixID_mvp = glGetUniformLocation(programID_current, "MVP");
	MatrixID_normal = glGetUniformLocation(programID_current, "NORMAL");
	TextureID = glGetUniformLocation(programID_current, "texture");
	TextureID = glGetUniformLocation(programID_tex, "texture");

	/*
	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(0, 3, 5), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 2.3f, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
		);

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);
	Model = glm::scale(Model, glm::vec3(20.0f, 20.0f, 20.0f));
	// Our ModelViewProjection : multiplication of our 3 matrices
	mvpMatrix = Projection * View * Model; // Remember, matrix multiplication is the other way around
	*/

	// Generate a buffer for the vertices
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * bunnyMesh.numVertex * 3, bunnyMesh.pVertexBuffer, GL_STATIC_DRAW);

	// Generate a buffer for the normals
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * bunnyMesh.numNormal * 3, bunnyMesh.pNormalBuffer, GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * bunnyMesh.numIndex * 3, bunnyMesh.pIndexBuffer, GL_STATIC_DRAW);

	// Generate a buffer for the bg vertex
	glGenBuffers(1, &bgvertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bgvertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bg_vertex_buffer), bg_vertex_buffer, GL_STATIC_DRAW);

	// Generate a buffer for the bg uv
	glGenBuffers(1, &bguvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bguvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bg_uv_buffer), bg_uv_buffer, GL_STATIC_DRAW);
	return;
}


void display()
{
	
	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(R * sin(Phi) * cos (Theta), R * sin (Theta),R * cos(Phi) * cos (Theta) ), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 2.3f, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
		);
	//printf("%f   %f",Phi,Theta);

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);
	Model = glm::scale(Model, glm::vec3(20.0f, 20.0f, 20.0f));
	// Our ModelViewProjection : multiplication of our 3 matrices
	mvpMatrix = Projection * View * Model; // Remember, matrix multiplication is the other way around

	// Calculate Normal Matrix
	//Model = glm::scale(Model, glm::vec3(1/20.0f, 1/20.0f, 1/20.0f));
	//normalMatrix = glm::mat3(View) * glm::mat3(Model);
	normalMatrix = View * Model;

	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//// first render the background
	glUseProgram(programID_tex);

	// Bind texture into Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture);

	// set 'texture' sampler to user Texture unit 0
	glUniform1i(TextureID, 0);
	
	// Vertex buffer for background
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, bgvertexbuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// UV buffer for the background
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, bguvbuffer);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw the background
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glClear(GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(programID_current); // bind the shader program
	
	// Pass the uniform parameter
	glUniformMatrix4fv(MatrixID_mvp, 1, GL_FALSE, &mvpMatrix[0][0]);	// pass the transform matrix
	glUniformMatrix4fv(MatrixID_normal, 1, GL_FALSE, &normalMatrix[0][0]);	// pass the transform matrix

	// activate the texture buffer
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture);
	glUniform1i(TextureID, 0);

	// Vertex buffer
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// Normal buffer
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void *) 0
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the mesh
	glDrawElements(
		GL_TRIANGLES,      // mode
		bunnyMesh.numIndex * 3,    // count
		GL_UNSIGNED_SHORT,   // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glutSwapBuffers();
}

void doIdle()
{
	glutPostRedisplay();
}

void main(int argc, char** argv)
{
	glutInit(&argc, argv);                         // Initialize GLUT.
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_RGBA | GLUT_MULTISAMPLE);   // Set display mode.
	glutInitWindowPosition(50, 100);   // Set top-left display-window position.
	glutInitWindowSize(400, 300);      // Set display-window width and height.
	glutCreateWindow("An Example OpenGL Program"); // Create display window.
	glutDisplayFunc(display);

	glutIdleFunc(doIdle);

	/* callback for mouse drags */
	glutMotionFunc(mouseMotionDrag);
	/* callback for window size changes */
	glutReshapeFunc(reshape);
	/* callback for mouse movement */
	glutPassiveMotionFunc(mouseMotion);
	/* callback for mouse button changes */
	glutMouseFunc(mouseButton);
	/* register for keyboard events */
	glutKeyboardFunc(keyboardFunc);
	/* do initialization */
	myinit();

	glutMainLoop();                    // Display everything and wait.
}


