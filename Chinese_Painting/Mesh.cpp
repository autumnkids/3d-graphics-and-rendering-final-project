#include "Mesh.h"


Mesh::Mesh()
{
	/* Initialize the pointer to NULL*/
	numVertex = 0;
	numNormal = 0;
	numIndex = 0;
	pVertexBuffer = NULL;
	pNormalBuffer = NULL;
	pIndexBuffer = NULL;
}


Mesh::~Mesh()
{
	/* Clean up the memory */
	delete pVertexBuffer;
	delete pNormalBuffer;
	delete pIndexBuffer;
}
