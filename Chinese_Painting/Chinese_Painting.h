#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "shader.hpp"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#define pi 3.141592653589793238462643383279 

// camera angles
extern double Theta;
extern double Phi;
extern double R;

// GPU IDs
extern GLuint programID_1;
extern GLuint programID_2;
extern GLuint programID_3;
extern GLuint programID_current;

// mouse control
extern int g_vMousePos[2];
extern int g_iLeftMouseButton,g_iMiddleMouseButton,g_iRightMouseButton;