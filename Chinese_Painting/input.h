
#ifndef _INPUT_H_
#define _INPUT_H_


void mouseMotionDrag(int x, int y);
void mouseMotion (int x, int y);
void mouseButton(int button, int state, int x, int y);
void keyboardFunc (unsigned char key, int x, int y);


void readWorld (char * fileName, struct world * jello);
void writeWorld (char * fileName, struct world * jello);

#endif

